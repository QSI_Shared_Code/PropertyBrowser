﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="All VI Server Class Get Properties" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="AbsTime.vi" Type="VI" URL="../All VI Server Class Get Properties/AbsTime.vi"/>
			<Item Name="AbsTimeConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/AbsTimeConstant.vi"/>
			<Item Name="AbstractDiagram.vi" Type="VI" URL="../All VI Server Class Get Properties/AbstractDiagram.vi"/>
			<Item Name="AbstractDynamicDispatch.vi" Type="VI" URL="../All VI Server Class Get Properties/AbstractDynamicDispatch.vi"/>
			<Item Name="ActiveXContainer.vi" Type="VI" URL="../All VI Server Class Get Properties/ActiveXContainer.vi"/>
			<Item Name="Array.vi" Type="VI" URL="../All VI Server Class Get Properties/Array.vi"/>
			<Item Name="ArrayConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/ArrayConstant.vi"/>
			<Item Name="ArrayToCluster.vi" Type="VI" URL="../All VI Server Class Get Properties/ArrayToCluster.vi"/>
			<Item Name="Boolean.vi" Type="VI" URL="../All VI Server Class Get Properties/Boolean.vi"/>
			<Item Name="BooleanConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/BooleanConstant.vi"/>
			<Item Name="BuildArray.vi" Type="VI" URL="../All VI Server Class Get Properties/BuildArray.vi"/>
			<Item Name="BuildClusterArray.vi" Type="VI" URL="../All VI Server Class Get Properties/BuildClusterArray.vi"/>
			<Item Name="BuildSpecification.vi" Type="VI" URL="../All VI Server Class Get Properties/BuildSpecification.vi"/>
			<Item Name="Bundler.vi" Type="VI" URL="../All VI Server Class Get Properties/Bundler.vi"/>
			<Item Name="Bus.vi" Type="VI" URL="../All VI Server Class Get Properties/Bus.vi"/>
			<Item Name="CallByRef.vi" Type="VI" URL="../All VI Server Class Get Properties/CallByRef.vi"/>
			<Item Name="CallLibrary.vi" Type="VI" URL="../All VI Server Class Get Properties/CallLibrary.vi"/>
			<Item Name="CallParentNode.vi" Type="VI" URL="../All VI Server Class Get Properties/CallParentNode.vi"/>
			<Item Name="CaseStructure.vi" Type="VI" URL="../All VI Server Class Get Properties/CaseStructure.vi"/>
			<Item Name="CIN.vi" Type="VI" URL="../All VI Server Class Get Properties/CIN.vi"/>
			<Item Name="ClassSpecifierConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/ClassSpecifierConstant.vi"/>
			<Item Name="Cluster.vi" Type="VI" URL="../All VI Server Class Get Properties/Cluster.vi"/>
			<Item Name="ClusterConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/ClusterConstant.vi"/>
			<Item Name="CodeWizard.vi" Type="VI" URL="../All VI Server Class Get Properties/CodeWizard.vi"/>
			<Item Name="ColorBox.vi" Type="VI" URL="../All VI Server Class Get Properties/ColorBox.vi"/>
			<Item Name="ColorBoxConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/ColorBoxConstant.vi"/>
			<Item Name="ColorGraphScale.vi" Type="VI" URL="../All VI Server Class Get Properties/ColorGraphScale.vi"/>
			<Item Name="ColorRamp.vi" Type="VI" URL="../All VI Server Class Get Properties/ColorRamp.vi"/>
			<Item Name="ColorScale.vi" Type="VI" URL="../All VI Server Class Get Properties/ColorScale.vi"/>
			<Item Name="ComboBox.vi" Type="VI" URL="../All VI Server Class Get Properties/ComboBox.vi"/>
			<Item Name="ComboBoxConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/ComboBoxConstant.vi"/>
			<Item Name="CompanionDiagram.vi" Type="VI" URL="../All VI Server Class Get Properties/CompanionDiagram.vi"/>
			<Item Name="Comparison.vi" Type="VI" URL="../All VI Server Class Get Properties/Comparison.vi"/>
			<Item Name="CompoundArithmetic.vi" Type="VI" URL="../All VI Server Class Get Properties/CompoundArithmetic.vi"/>
			<Item Name="ConditionalTunnel.vi" Type="VI" URL="../All VI Server Class Get Properties/ConditionalTunnel.vi"/>
			<Item Name="ConfNode.vi" Type="VI" URL="../All VI Server Class Get Properties/ConfNode.vi"/>
			<Item Name="ConnectorPane.vi" Type="VI" URL="../All VI Server Class Get Properties/ConnectorPane.vi"/>
			<Item Name="Constant.vi" Type="VI" URL="../All VI Server Class Get Properties/Constant.vi"/>
			<Item Name="Constructor.vi" Type="VI" URL="../All VI Server Class Get Properties/Constructor.vi"/>
			<Item Name="Control.vi" Type="VI" URL="../All VI Server Class Get Properties/Control.vi"/>
			<Item Name="ControlReferenceConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/ControlReferenceConstant.vi"/>
			<Item Name="ControlTerminal.vi" Type="VI" URL="../All VI Server Class Get Properties/ControlTerminal.vi"/>
			<Item Name="CopyConflict.vi" Type="VI" URL="../All VI Server Class Get Properties/CopyConflict.vi"/>
			<Item Name="CopyExpert.vi" Type="VI" URL="../All VI Server Class Get Properties/CopyExpert.vi"/>
			<Item Name="CopyItem.vi" Type="VI" URL="../All VI Server Class Get Properties/CopyItem.vi"/>
			<Item Name="CopyOperation.vi" Type="VI" URL="../All VI Server Class Get Properties/CopyOperation.vi"/>
			<Item Name="Cursor.vi" Type="VI" URL="../All VI Server Class Get Properties/Cursor.vi"/>
			<Item Name="DAQChannelName.vi" Type="VI" URL="../All VI Server Class Get Properties/DAQChannelName.vi"/>
			<Item Name="DAQChannelNameConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/DAQChannelNameConstant.vi"/>
			<Item Name="DAQmxName.vi" Type="VI" URL="../All VI Server Class Get Properties/DAQmxName.vi"/>
			<Item Name="DAQmxNameConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/DAQmxNameConstant.vi"/>
			<Item Name="DataValRefNum.vi" Type="VI" URL="../All VI Server Class Get Properties/DataValRefNum.vi"/>
			<Item Name="DataValRefNumConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/DataValRefNumConstant.vi"/>
			<Item Name="Decoration.vi" Type="VI" URL="../All VI Server Class Get Properties/Decoration.vi"/>
			<Item Name="Diagram.vi" Type="VI" URL="../All VI Server Class Get Properties/Diagram.vi"/>
			<Item Name="Digital.vi" Type="VI" URL="../All VI Server Class Get Properties/Digital.vi"/>
			<Item Name="DigitalGraph.vi" Type="VI" URL="../All VI Server Class Get Properties/DigitalGraph.vi"/>
			<Item Name="DigitalNumericConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/DigitalNumericConstant.vi"/>
			<Item Name="DigitalTable.vi" Type="VI" URL="../All VI Server Class Get Properties/DigitalTable.vi"/>
			<Item Name="DisableStructure.vi" Type="VI" URL="../All VI Server Class Get Properties/DisableStructure.vi"/>
			<Item Name="DSCTag.vi" Type="VI" URL="../All VI Server Class Get Properties/DSCTag.vi"/>
			<Item Name="DSCTagConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/DSCTagConstant.vi"/>
			<Item Name="DynamicDispatchSubVI.vi" Type="VI" URL="../All VI Server Class Get Properties/DynamicDispatchSubVI.vi"/>
			<Item Name="Enum.vi" Type="VI" URL="../All VI Server Class Get Properties/Enum.vi"/>
			<Item Name="EnumConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/EnumConstant.vi"/>
			<Item Name="EventStructure.vi" Type="VI" URL="../All VI Server Class Get Properties/EventStructure.vi"/>
			<Item Name="ExpressionNode.vi" Type="VI" URL="../All VI Server Class Get Properties/ExpressionNode.vi"/>
			<Item Name="ExternalNode.vi" Type="VI" URL="../All VI Server Class Get Properties/ExternalNode.vi"/>
			<Item Name="FacadeVI.vi" Type="VI" URL="../All VI Server Class Get Properties/FacadeVI.vi"/>
			<Item Name="FeedbackNode.vi" Type="VI" URL="../All VI Server Class Get Properties/FeedbackNode.vi"/>
			<Item Name="FileDialog.vi" Type="VI" URL="../All VI Server Class Get Properties/FileDialog.vi"/>
			<Item Name="FixedConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/FixedConstant.vi"/>
			<Item Name="FlatSequence.vi" Type="VI" URL="../All VI Server Class Get Properties/FlatSequence.vi"/>
			<Item Name="FlatSequenceFrame.vi" Type="VI" URL="../All VI Server Class Get Properties/FlatSequenceFrame.vi"/>
			<Item Name="FlatSequenceInnerTunnel.vi" Type="VI" URL="../All VI Server Class Get Properties/FlatSequenceInnerTunnel.vi"/>
			<Item Name="FlatSequenceOuterTunnel.vi" Type="VI" URL="../All VI Server Class Get Properties/FlatSequenceOuterTunnel.vi"/>
			<Item Name="FlattenString.vi" Type="VI" URL="../All VI Server Class Get Properties/FlattenString.vi"/>
			<Item Name="FlattenUnflattenString.vi" Type="VI" URL="../All VI Server Class Get Properties/FlattenUnflattenString.vi"/>
			<Item Name="ForkNode.vi" Type="VI" URL="../All VI Server Class Get Properties/ForkNode.vi"/>
			<Item Name="ForLoop.vi" Type="VI" URL="../All VI Server Class Get Properties/ForLoop.vi"/>
			<Item Name="FormatScanString.vi" Type="VI" URL="../All VI Server Class Get Properties/FormatScanString.vi"/>
			<Item Name="Formula.vi" Type="VI" URL="../All VI Server Class Get Properties/Formula.vi"/>
			<Item Name="FormulaParameter.vi" Type="VI" URL="../All VI Server Class Get Properties/FormulaParameter.vi"/>
			<Item Name="Function.vi" Type="VI" URL="../All VI Server Class Get Properties/Function.vi"/>
			<Item Name="GenClassRef.vi" Type="VI" URL="../All VI Server Class Get Properties/GenClassRef.vi"/>
			<Item Name="GenClassRefConst.vi" Type="VI" URL="../All VI Server Class Get Properties/GenClassRefConst.vi"/>
			<Item Name="GenClassTagFlatRef.vi" Type="VI" URL="../All VI Server Class Get Properties/GenClassTagFlatRef.vi"/>
			<Item Name="GenClassTagFlatRefConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/GenClassTagFlatRefConstant.vi"/>
			<Item Name="GenClassTagRef.vi" Type="VI" URL="../All VI Server Class Get Properties/GenClassTagRef.vi"/>
			<Item Name="GenClassTagRefConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/GenClassTagRefConstant.vi"/>
			<Item Name="Generic.vi" Type="VI" URL="../All VI Server Class Get Properties/Generic.vi"/>
			<Item Name="GenericSubVI.vi" Type="VI" URL="../All VI Server Class Get Properties/GenericSubVI.vi"/>
			<Item Name="Global.vi" Type="VI" URL="../All VI Server Class Get Properties/Global.vi"/>
			<Item Name="GObject.vi" Type="VI" URL="../All VI Server Class Get Properties/GObject.vi"/>
			<Item Name="GPIBReadWrite.vi" Type="VI" URL="../All VI Server Class Get Properties/GPIBReadWrite.vi"/>
			<Item Name="GraphChart.vi" Type="VI" URL="../All VI Server Class Get Properties/GraphChart.vi"/>
			<Item Name="GraphScale.vi" Type="VI" URL="../All VI Server Class Get Properties/GraphScale.vi"/>
			<Item Name="GrowableFunction.vi" Type="VI" URL="../All VI Server Class Get Properties/GrowableFunction.vi"/>
			<Item Name="IndexArray.vi" Type="VI" URL="../All VI Server Class Get Properties/IndexArray.vi"/>
			<Item Name="InlineCNode.vi" Type="VI" URL="../All VI Server Class Get Properties/InlineCNode.vi"/>
			<Item Name="InnerTerminal.vi" Type="VI" URL="../All VI Server Class Get Properties/InnerTerminal.vi"/>
			<Item Name="InPlaceArrayNode.vi" Type="VI" URL="../All VI Server Class Get Properties/InPlaceArrayNode.vi"/>
			<Item Name="InPlaceArraySplitNode.vi" Type="VI" URL="../All VI Server Class Get Properties/InPlaceArraySplitNode.vi"/>
			<Item Name="InPlaceBorderNode.vi" Type="VI" URL="../All VI Server Class Get Properties/InPlaceBorderNode.vi"/>
			<Item Name="InPlaceClusterNode.vi" Type="VI" URL="../All VI Server Class Get Properties/InPlaceClusterNode.vi"/>
			<Item Name="InPlaceDataValRefNode.vi" Type="VI" URL="../All VI Server Class Get Properties/InPlaceDataValRefNode.vi"/>
			<Item Name="InPlaceElementNode.vi" Type="VI" URL="../All VI Server Class Get Properties/InPlaceElementNode.vi"/>
			<Item Name="InPlaceElementStructure.vi" Type="VI" URL="../All VI Server Class Get Properties/InPlaceElementStructure.vi"/>
			<Item Name="InPlaceVariantNode.vi" Type="VI" URL="../All VI Server Class Get Properties/InPlaceVariantNode.vi"/>
			<Item Name="InRangeAndCoerce.vi" Type="VI" URL="../All VI Server Class Get Properties/InRangeAndCoerce.vi"/>
			<Item Name="IntensityChart.vi" Type="VI" URL="../All VI Server Class Get Properties/IntensityChart.vi"/>
			<Item Name="IntensityGraph.vi" Type="VI" URL="../All VI Server Class Get Properties/IntensityGraph.vi"/>
			<Item Name="Invoke.vi" Type="VI" URL="../All VI Server Class Get Properties/Invoke.vi"/>
			<Item Name="IOName.vi" Type="VI" URL="../All VI Server Class Get Properties/IOName.vi"/>
			<Item Name="IONameConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/IONameConstant.vi"/>
			<Item Name="IVILogicalName.vi" Type="VI" URL="../All VI Server Class Get Properties/IVILogicalName.vi"/>
			<Item Name="IVILogicalNameConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/IVILogicalNameConstant.vi"/>
			<Item Name="JoinNode.vi" Type="VI" URL="../All VI Server Class Get Properties/JoinNode.vi"/>
			<Item Name="JunctionNode.vi" Type="VI" URL="../All VI Server Class Get Properties/JunctionNode.vi"/>
			<Item Name="Knob.vi" Type="VI" URL="../All VI Server Class Get Properties/Knob.vi"/>
			<Item Name="LabVIEWClassConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/LabVIEWClassConstant.vi"/>
			<Item Name="LabVIEWClassControl.vi" Type="VI" URL="../All VI Server Class Get Properties/LabVIEWClassControl.vi"/>
			<Item Name="LeftShiftRegister.vi" Type="VI" URL="../All VI Server Class Get Properties/LeftShiftRegister.vi"/>
			<Item Name="Library.vi" Type="VI" URL="../All VI Server Class Get Properties/Library.vi"/>
			<Item Name="LibraryData.vi" Type="VI" URL="../All VI Server Class Get Properties/LibraryData.vi"/>
			<Item Name="ListBox.vi" Type="VI" URL="../All VI Server Class Get Properties/ListBox.vi"/>
			<Item Name="Local.vi" Type="VI" URL="../All VI Server Class Get Properties/Local.vi"/>
			<Item Name="Loop.vi" Type="VI" URL="../All VI Server Class Get Properties/Loop.vi"/>
			<Item Name="LoopTunnel.vi" Type="VI" URL="../All VI Server Class Get Properties/LoopTunnel.vi"/>
			<Item Name="LVClassLibrary.vi" Type="VI" URL="../All VI Server Class Get Properties/LVClassLibrary.vi"/>
			<Item Name="LVClassPropDefFolder.vi" Type="VI" URL="../All VI Server Class Get Properties/LVClassPropDefFolder.vi"/>
			<Item Name="LVObjectRefNum.vi" Type="VI" URL="../All VI Server Class Get Properties/LVObjectRefNum.vi"/>
			<Item Name="LVTarget.vi" Type="VI" URL="../All VI Server Class Get Properties/LVTarget.vi"/>
			<Item Name="LVVariant.vi" Type="VI" URL="../All VI Server Class Get Properties/LVVariant.vi"/>
			<Item Name="Map.vi" Type="VI" URL="../All VI Server Class Get Properties/Map.vi"/>
			<Item Name="MapConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/MapConstant.vi"/>
			<Item Name="MasterWizard.vi" Type="VI" URL="../All VI Server Class Get Properties/MasterWizard.vi"/>
			<Item Name="MathDiagram.vi" Type="VI" URL="../All VI Server Class Get Properties/MathDiagram.vi"/>
			<Item Name="MathScriptCallByRef.vi" Type="VI" URL="../All VI Server Class Get Properties/MathScriptCallByRef.vi"/>
			<Item Name="MathScriptNode.vi" Type="VI" URL="../All VI Server Class Get Properties/MathScriptNode.vi"/>
			<Item Name="MathScriptNodeParameter.vi" Type="VI" URL="../All VI Server Class Get Properties/MathScriptNodeParameter.vi"/>
			<Item Name="MethodVI.vi" Type="VI" URL="../All VI Server Class Get Properties/MethodVI.vi"/>
			<Item Name="MixedCheckbox.vi" Type="VI" URL="../All VI Server Class Get Properties/MixedCheckbox.vi"/>
			<Item Name="MixedSignalGraph.vi" Type="VI" URL="../All VI Server Class Get Properties/MixedSignalGraph.vi"/>
			<Item Name="MulticolumnListbox.vi" Type="VI" URL="../All VI Server Class Get Properties/MulticolumnListbox.vi"/>
			<Item Name="MultiFrameStructure.vi" Type="VI" URL="../All VI Server Class Get Properties/MultiFrameStructure.vi"/>
			<Item Name="MultiSegmentPipe.vi" Type="VI" URL="../All VI Server Class Get Properties/MultiSegmentPipe.vi"/>
			<Item Name="NamedBundler.vi" Type="VI" URL="../All VI Server Class Get Properties/NamedBundler.vi"/>
			<Item Name="NamedNumeric.vi" Type="VI" URL="../All VI Server Class Get Properties/NamedNumeric.vi"/>
			<Item Name="NamedNumericConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/NamedNumericConstant.vi"/>
			<Item Name="NamedUnbundler.vi" Type="VI" URL="../All VI Server Class Get Properties/NamedUnbundler.vi"/>
			<Item Name="Node.vi" Type="VI" URL="../All VI Server Class Get Properties/Node.vi"/>
			<Item Name="Numeric.vi" Type="VI" URL="../All VI Server Class Get Properties/Numeric.vi"/>
			<Item Name="NumericConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/NumericConstant.vi"/>
			<Item Name="NumericText.vi" Type="VI" URL="../All VI Server Class Get Properties/NumericText.vi"/>
			<Item Name="NumericWithScale.vi" Type="VI" URL="../All VI Server Class Get Properties/NumericWithScale.vi"/>
			<Item Name="ObjectFunction.vi" Type="VI" URL="../All VI Server Class Get Properties/ObjectFunction.vi"/>
			<Item Name="OuterTerminal.vi" Type="VI" URL="../All VI Server Class Get Properties/OuterTerminal.vi"/>
			<Item Name="OverridableParameterTerminal.vi" Type="VI" URL="../All VI Server Class Get Properties/OverridableParameterTerminal.vi"/>
			<Item Name="Page.vi" Type="VI" URL="../All VI Server Class Get Properties/Page.vi"/>
			<Item Name="PageSelector.vi" Type="VI" URL="../All VI Server Class Get Properties/PageSelector.vi"/>
			<Item Name="Pane.vi" Type="VI" URL="../All VI Server Class Get Properties/Pane.vi"/>
			<Item Name="Panel.vi" Type="VI" URL="../All VI Server Class Get Properties/Panel.vi"/>
			<Item Name="ParameterTerminal.vi" Type="VI" URL="../All VI Server Class Get Properties/ParameterTerminal.vi"/>
			<Item Name="Path.vi" Type="VI" URL="../All VI Server Class Get Properties/Path.vi"/>
			<Item Name="PathConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/PathConstant.vi"/>
			<Item Name="Picture.vi" Type="VI" URL="../All VI Server Class Get Properties/Picture.vi"/>
			<Item Name="Pixmap.vi" Type="VI" URL="../All VI Server Class Get Properties/Pixmap.vi"/>
			<Item Name="Plot.vi" Type="VI" URL="../All VI Server Class Get Properties/Plot.vi"/>
			<Item Name="PlugInControl.vi" Type="VI" URL="../All VI Server Class Get Properties/PlugInControl.vi"/>
			<Item Name="PlugInDDODummyContainer.vi" Type="VI" URL="../All VI Server Class Get Properties/PlugInDDODummyContainer.vi"/>
			<Item Name="PolymorphicSubVI.vi" Type="VI" URL="../All VI Server Class Get Properties/PolymorphicSubVI.vi"/>
			<Item Name="PolymorphicVI.vi" Type="VI" URL="../All VI Server Class Get Properties/PolymorphicVI.vi"/>
			<Item Name="PolymorphicVISelector.vi" Type="VI" URL="../All VI Server Class Get Properties/PolymorphicVISelector.vi"/>
			<Item Name="Probe.vi" Type="VI" URL="../All VI Server Class Get Properties/Probe.vi"/>
			<Item Name="Project.vi" Type="VI" URL="../All VI Server Class Get Properties/Project.vi"/>
			<Item Name="ProjectFilesViewItem.vi" Type="VI" URL="../All VI Server Class Get Properties/ProjectFilesViewItem.vi"/>
			<Item Name="ProjectItem.vi" Type="VI" URL="../All VI Server Class Get Properties/ProjectItem.vi"/>
			<Item Name="ProjectItemType.vi" Type="VI" URL="../All VI Server Class Get Properties/ProjectItemType.vi"/>
			<Item Name="Property.vi" Type="VI" URL="../All VI Server Class Get Properties/Property.vi"/>
			<Item Name="PropertyFolder.vi" Type="VI" URL="../All VI Server Class Get Properties/PropertyFolder.vi"/>
			<Item Name="PropertyItem.vi" Type="VI" URL="../All VI Server Class Get Properties/PropertyItem.vi"/>
			<Item Name="Provider.vi" Type="VI" URL="../All VI Server Class Get Properties/Provider.vi"/>
			<Item Name="RadioButtonsControl.vi" Type="VI" URL="../All VI Server Class Get Properties/RadioButtonsControl.vi"/>
			<Item Name="ReadWriteFile.vi" Type="VI" URL="../All VI Server Class Get Properties/ReadWriteFile.vi"/>
			<Item Name="RefNum.vi" Type="VI" URL="../All VI Server Class Get Properties/RefNum.vi"/>
			<Item Name="RefNumConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/RefNumConstant.vi"/>
			<Item Name="RegionNode.vi" Type="VI" URL="../All VI Server Class Get Properties/RegionNode.vi"/>
			<Item Name="RegionTunnel.vi" Type="VI" URL="../All VI Server Class Get Properties/RegionTunnel.vi"/>
			<Item Name="RegisterForEvents.vi" Type="VI" URL="../All VI Server Class Get Properties/RegisterForEvents.vi"/>
			<Item Name="RightShiftRegister.vi" Type="VI" URL="../All VI Server Class Get Properties/RightShiftRegister.vi"/>
			<Item Name="Ring.vi" Type="VI" URL="../All VI Server Class Get Properties/Ring.vi"/>
			<Item Name="RingConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/RingConstant.vi"/>
			<Item Name="RotaryColorScale.vi" Type="VI" URL="../All VI Server Class Get Properties/RotaryColorScale.vi"/>
			<Item Name="Scale.vi" Type="VI" URL="../All VI Server Class Get Properties/Scale.vi"/>
			<Item Name="ScDiagram.vi" Type="VI" URL="../All VI Server Class Get Properties/ScDiagram.vi"/>
			<Item Name="SceneBox.vi" Type="VI" URL="../All VI Server Class Get Properties/SceneBox.vi"/>
			<Item Name="SceneClipPlane.vi" Type="VI" URL="../All VI Server Class Get Properties/SceneClipPlane.vi"/>
			<Item Name="SceneCone.vi" Type="VI" URL="../All VI Server Class Get Properties/SceneCone.vi"/>
			<Item Name="SceneCylinder.vi" Type="VI" URL="../All VI Server Class Get Properties/SceneCylinder.vi"/>
			<Item Name="SceneGeometry.vi" Type="VI" URL="../All VI Server Class Get Properties/SceneGeometry.vi"/>
			<Item Name="SceneGraphDisplay.vi" Type="VI" URL="../All VI Server Class Get Properties/SceneGraphDisplay.vi"/>
			<Item Name="SceneHeightField.vi" Type="VI" URL="../All VI Server Class Get Properties/SceneHeightField.vi"/>
			<Item Name="SceneLight.vi" Type="VI" URL="../All VI Server Class Get Properties/SceneLight.vi"/>
			<Item Name="SceneMesh.vi" Type="VI" URL="../All VI Server Class Get Properties/SceneMesh.vi"/>
			<Item Name="SceneNode.vi" Type="VI" URL="../All VI Server Class Get Properties/SceneNode.vi"/>
			<Item Name="SceneObject.vi" Type="VI" URL="../All VI Server Class Get Properties/SceneObject.vi"/>
			<Item Name="SceneSphere.vi" Type="VI" URL="../All VI Server Class Get Properties/SceneSphere.vi"/>
			<Item Name="SceneText.vi" Type="VI" URL="../All VI Server Class Get Properties/SceneText.vi"/>
			<Item Name="SceneTexture.vi" Type="VI" URL="../All VI Server Class Get Properties/SceneTexture.vi"/>
			<Item Name="SceneWindow.vi" Type="VI" URL="../All VI Server Class Get Properties/SceneWindow.vi"/>
			<Item Name="ScriptNode.vi" Type="VI" URL="../All VI Server Class Get Properties/ScriptNode.vi"/>
			<Item Name="ScriptNodeParameter.vi" Type="VI" URL="../All VI Server Class Get Properties/ScriptNodeParameter.vi"/>
			<Item Name="Scrollbar.vi" Type="VI" URL="../All VI Server Class Get Properties/Scrollbar.vi"/>
			<Item Name="SDF Companion Diagram.vi" Type="VI" URL="../All VI Server Class Get Properties/SDF Companion Diagram.vi"/>
			<Item Name="SDFDiagram.vi" Type="VI" URL="../All VI Server Class Get Properties/SDFDiagram.vi"/>
			<Item Name="SelectorTunnel.vi" Type="VI" URL="../All VI Server Class Get Properties/SelectorTunnel.vi"/>
			<Item Name="Sequence.vi" Type="VI" URL="../All VI Server Class Get Properties/Sequence.vi"/>
			<Item Name="SequenceLocal.vi" Type="VI" URL="../All VI Server Class Get Properties/SequenceLocal.vi"/>
			<Item Name="Set.vi" Type="VI" URL="../All VI Server Class Get Properties/Set.vi"/>
			<Item Name="SetConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/SetConstant.vi"/>
			<Item Name="SharedVariableDynamicOpen.vi" Type="VI" URL="../All VI Server Class Get Properties/SharedVariableDynamicOpen.vi"/>
			<Item Name="SharedVariableDynamicRead.vi" Type="VI" URL="../All VI Server Class Get Properties/SharedVariableDynamicRead.vi"/>
			<Item Name="SharedVariableDynamicWrite.vi" Type="VI" URL="../All VI Server Class Get Properties/SharedVariableDynamicWrite.vi"/>
			<Item Name="SharedVariableNode.vi" Type="VI" URL="../All VI Server Class Get Properties/SharedVariableNode.vi"/>
			<Item Name="SimDiagram.vi" Type="VI" URL="../All VI Server Class Get Properties/SimDiagram.vi"/>
			<Item Name="SimulationDCO.vi" Type="VI" URL="../All VI Server Class Get Properties/SimulationDCO.vi"/>
			<Item Name="SimulationNode.vi" Type="VI" URL="../All VI Server Class Get Properties/SimulationNode.vi"/>
			<Item Name="Slide.vi" Type="VI" URL="../All VI Server Class Get Properties/Slide.vi"/>
			<Item Name="SlideScale.vi" Type="VI" URL="../All VI Server Class Get Properties/SlideScale.vi"/>
			<Item Name="Splitter.vi" Type="VI" URL="../All VI Server Class Get Properties/Splitter.vi"/>
			<Item Name="StatechartLibrary.vi" Type="VI" URL="../All VI Server Class Get Properties/StatechartLibrary.vi"/>
			<Item Name="StatechartStructureNode.vi" Type="VI" URL="../All VI Server Class Get Properties/StatechartStructureNode.vi"/>
			<Item Name="StateDiagramWizard.vi" Type="VI" URL="../All VI Server Class Get Properties/StateDiagramWizard.vi"/>
			<Item Name="StateNode.vi" Type="VI" URL="../All VI Server Class Get Properties/StateNode.vi"/>
			<Item Name="StaticVIReference.vi" Type="VI" URL="../All VI Server Class Get Properties/StaticVIReference.vi"/>
			<Item Name="String.vi" Type="VI" URL="../All VI Server Class Get Properties/String.vi"/>
			<Item Name="StringConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/StringConstant.vi"/>
			<Item Name="Structure.vi" Type="VI" URL="../All VI Server Class Get Properties/Structure.vi"/>
			<Item Name="StubDDO.vi" Type="VI" URL="../All VI Server Class Get Properties/StubDDO.vi"/>
			<Item Name="SubPanel.vi" Type="VI" URL="../All VI Server Class Get Properties/SubPanel.vi"/>
			<Item Name="SubsystemVI.vi" Type="VI" URL="../All VI Server Class Get Properties/SubsystemVI.vi"/>
			<Item Name="SubVI.vi" Type="VI" URL="../All VI Server Class Get Properties/SubVI.vi"/>
			<Item Name="SubWizard.vi" Type="VI" URL="../All VI Server Class Get Properties/SubWizard.vi"/>
			<Item Name="SynchronousDataFlowDCO.vi" Type="VI" URL="../All VI Server Class Get Properties/SynchronousDataFlowDCO.vi"/>
			<Item Name="SynchronousDataFlowNode.vi" Type="VI" URL="../All VI Server Class Get Properties/SynchronousDataFlowNode.vi"/>
			<Item Name="TabControl.vi" Type="VI" URL="../All VI Server Class Get Properties/TabControl.vi"/>
			<Item Name="Table.vi" Type="VI" URL="../All VI Server Class Get Properties/Table.vi"/>
			<Item Name="TargetItem.vi" Type="VI" URL="../All VI Server Class Get Properties/TargetItem.vi"/>
			<Item Name="Terminal.vi" Type="VI" URL="../All VI Server Class Get Properties/Terminal.vi"/>
			<Item Name="Text.vi" Type="VI" URL="../All VI Server Class Get Properties/Text.vi"/>
			<Item Name="TextBaseNode.vi" Type="VI" URL="../All VI Server Class Get Properties/TextBaseNode.vi"/>
			<Item Name="TimedLoop.vi" Type="VI" URL="../All VI Server Class Get Properties/TimedLoop.vi"/>
			<Item Name="TimedSequence.vi" Type="VI" URL="../All VI Server Class Get Properties/TimedSequence.vi"/>
			<Item Name="TimedStructDCO.vi" Type="VI" URL="../All VI Server Class Get Properties/TimedStructDCO.vi"/>
			<Item Name="TimeFlatSequence.vi" Type="VI" URL="../All VI Server Class Get Properties/TimeFlatSequence.vi"/>
			<Item Name="TimeSequenceFrame.vi" Type="VI" URL="../All VI Server Class Get Properties/TimeSequenceFrame.vi"/>
			<Item Name="TopLevelDiagram.vi" Type="VI" URL="../All VI Server Class Get Properties/TopLevelDiagram.vi"/>
			<Item Name="TreeControl.vi" Type="VI" URL="../All VI Server Class Get Properties/TreeControl.vi"/>
			<Item Name="Tunnel.vi" Type="VI" URL="../All VI Server Class Get Properties/Tunnel.vi"/>
			<Item Name="TypeCast.vi" Type="VI" URL="../All VI Server Class Get Properties/TypeCast.vi"/>
			<Item Name="TypedRefNum.vi" Type="VI" URL="../All VI Server Class Get Properties/TypedRefNum.vi"/>
			<Item Name="TypedRefNumConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/TypedRefNumConstant.vi"/>
			<Item Name="Unbundler.vi" Type="VI" URL="../All VI Server Class Get Properties/Unbundler.vi"/>
			<Item Name="UnitCast.vi" Type="VI" URL="../All VI Server Class Get Properties/UnitCast.vi"/>
			<Item Name="Variable.vi" Type="VI" URL="../All VI Server Class Get Properties/Variable.vi"/>
			<Item Name="VI.vi" Type="VI" URL="../All VI Server Class Get Properties/VI.vi"/>
			<Item Name="VIRefNum.vi" Type="VI" URL="../All VI Server Class Get Properties/VIRefNum.vi"/>
			<Item Name="VISAResourceName.vi" Type="VI" URL="../All VI Server Class Get Properties/VISAResourceName.vi"/>
			<Item Name="VISAResourceNameConstant.vi" Type="VI" URL="../All VI Server Class Get Properties/VISAResourceNameConstant.vi"/>
			<Item Name="WaveformChart.vi" Type="VI" URL="../All VI Server Class Get Properties/WaveformChart.vi"/>
			<Item Name="WaveformData.vi" Type="VI" URL="../All VI Server Class Get Properties/WaveformData.vi"/>
			<Item Name="WaveformGraph.vi" Type="VI" URL="../All VI Server Class Get Properties/WaveformGraph.vi"/>
			<Item Name="WhileLoop.vi" Type="VI" URL="../All VI Server Class Get Properties/WhileLoop.vi"/>
			<Item Name="Wire.vi" Type="VI" URL="../All VI Server Class Get Properties/Wire.vi"/>
			<Item Name="XControlLibrary.vi" Type="VI" URL="../All VI Server Class Get Properties/XControlLibrary.vi"/>
			<Item Name="XDataNode.vi" Type="VI" URL="../All VI Server Class Get Properties/XDataNode.vi"/>
			<Item Name="XInterfaceLibrary.vi" Type="VI" URL="../All VI Server Class Get Properties/XInterfaceLibrary.vi"/>
			<Item Name="XNode.vi" Type="VI" URL="../All VI Server Class Get Properties/XNode.vi"/>
			<Item Name="XNodeLibrary.vi" Type="VI" URL="../All VI Server Class Get Properties/XNodeLibrary.vi"/>
			<Item Name="XPropertyFolder.vi" Type="VI" URL="../All VI Server Class Get Properties/XPropertyFolder.vi"/>
			<Item Name="XYGraph.vi" Type="VI" URL="../All VI Server Class Get Properties/XYGraph.vi"/>
		</Item>
		<Item Name="Data Type Popups" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="Boolean Colors Popup.lvclass" Type="LVClass" URL="../Popup Classes/Boolean Colors/Boolean Colors Popup.lvclass"/>
			<Item Name="Boolean Popup.lvclass" Type="LVClass" URL="../Popup Classes/Boolean/Boolean Popup.lvclass"/>
			<Item Name="Color Popup.lvclass" Type="LVClass" URL="../Popup Classes/Color/Color Popup.lvclass"/>
			<Item Name="Generic Popup.lvclass" Type="LVClass" URL="../Popup Classes/Generic/Generic Popup.lvclass"/>
			<Item Name="List Popup.lvclass" Type="LVClass" URL="../Popup Classes/List/List Popup.lvclass"/>
			<Item Name="Numeric Popup.lvclass" Type="LVClass" URL="../Popup Classes/Numeric/Numeric Popup.lvclass"/>
			<Item Name="Path Popup.lvclass" Type="LVClass" URL="../Popup Classes/Path/Path Popup.lvclass"/>
			<Item Name="String Popup.lvclass" Type="LVClass" URL="../Popup Classes/String/String Popup.lvclass"/>
		</Item>
		<Item Name="Script Set Property" Type="Folder">
			<Item Name="Set Property Script (ProjectItem).vi" Type="VI" URL="../Main Code/Set Property Script (ProjectItem).vi"/>
			<Item Name="Set Property Script VI (ProjectItem).vi" Type="VI" URL="../Main Code/Set Property Script VI (ProjectItem).vi"/>
			<Item Name="Set Property Script VI.vi" Type="VI" URL="../Main Code/Set Property Script VI.vi"/>
			<Item Name="Set Property Script.vi" Type="VI" URL="../Main Code/Set Property Script.vi"/>
		</Item>
		<Item Name="Graphics" Type="Folder">
			<Item Name="read and write.png" Type="Document" URL="../Graphics/read and write.png"/>
			<Item Name="read.png" Type="Document" URL="../Graphics/read.png"/>
			<Item Name="write.png" Type="Document" URL="../Graphics/write.png"/>
		</Item>
		<Item Name="Property Browser.lvclass" Type="LVClass" URL="../Main Code/Property Browser.lvclass"/>
		<Item Name="Property Browser QD.vi" Type="VI" URL="../Property Browser QD.vi"/>
		<Item Name="QSI Property Browser.vi" Type="VI" URL="../QSI Property Browser.vi"/>
		<Item Name="Post-Install Custom Action.vi" Type="VI" URL="../Post-Install Custom Action.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="ASC_ArrayOfUniqueIDStringParentIDStringName.ctl" Type="VI" URL="/&lt;vilib&gt;/_script/All Supported PropertiesOrMethods TypeDefs/ASC_ArrayOfUniqueIDStringParentIDStringName.ctl"/>
				<Item Name="ASC_UniqueIDStringParentIDStringName.ctl" Type="VI" URL="/&lt;vilib&gt;/_script/All Supported PropertiesOrMethods TypeDefs/ASC_UniqueIDStringParentIDStringName.ctl"/>
				<Item Name="ASPM_ArrayOfUniqueIDStringDatanameShortnameLongname.ctl" Type="VI" URL="/&lt;vilib&gt;/_script/All Supported PropertiesOrMethods TypeDefs/ASPM_ArrayOfUniqueIDStringDatanameShortnameLongname.ctl"/>
				<Item Name="ASPM_UniqueIDStringDatanameShortnameLongname.ctl" Type="VI" URL="/&lt;vilib&gt;/_script/All Supported PropertiesOrMethods TypeDefs/ASPM_UniqueIDStringDatanameShortnameLongname.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LV3DPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LV3DPointTypeDef.ctl"/>
				<Item Name="LVAnnotationListTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVAnnotationListTypeDef.ctl"/>
				<Item Name="LVAttenuationTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVAttenuationTypeDef.ctl"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVBreakpointStatusEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBreakpointStatusEnum.ctl"/>
				<Item Name="LVBrowseOptionsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBrowseOptionsTypeDef.ctl"/>
				<Item Name="LVComboBoxStrsAndValuesArrayTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVComboBoxStrsAndValuesArrayTypeDef.ctl"/>
				<Item Name="LVCursorListTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVCursorListTypeDef.ctl"/>
				<Item Name="LVDataSocketStatusTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDataSocketStatusTypeDef.ctl"/>
				<Item Name="lveventtype.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/lveventtype.ctl"/>
				<Item Name="LVFixedPointOverflowPolicyTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/fxp/LVFixedPointOverflowPolicyTypeDef.ctl"/>
				<Item Name="LVFixedPointQuantizationPolicyTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/fxp/LVFixedPointQuantizationPolicyTypeDef.ctl"/>
				<Item Name="LVFixedPointRepBitsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/fxp/LVFixedPointRepBitsTypeDef.ctl"/>
				<Item Name="LVFixedPointRepRangeTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/fxp/LVFixedPointRepRangeTypeDef.ctl"/>
				<Item Name="LVFontTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVFontTypeDef.ctl"/>
				<Item Name="LVForegroundBackgroundColorsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVForegroundBackgroundColorsTypeDef.ctl"/>
				<Item Name="LVFormatAndPrecisionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVFormatAndPrecisionTypeDef.ctl"/>
				<Item Name="LVKeyNavTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVKeyNavTypeDef.ctl"/>
				<Item Name="LVKeyTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVKeyTypeDef.ctl"/>
				<Item Name="LVLastCompiledWithTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVLastCompiledWithTypeDef.ctl"/>
				<Item Name="LVMajorAndMinorColorsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVMajorAndMinorColorsTypeDef.ctl"/>
				<Item Name="LVMinMaxIncTimestampTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVMinMaxIncTimestampTypeDef.ctl"/>
				<Item Name="LVMinMaxIncTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVMinMaxIncTypeDef.ctl"/>
				<Item Name="LVMouseTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVMouseTypeDef.ctl"/>
				<Item Name="LVNodeAndColumnTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVNodeAndColumnTypeDef.ctl"/>
				<Item Name="LVNumericOverridePolicyTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/fxp/LVNumericOverridePolicyTypeDef.ctl"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="LVOffsetAndMultiplierTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVOffsetAndMultiplierTypeDef.ctl"/>
				<Item Name="LVOutOfRangeActionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVOutOfRangeActionTypeDef.ctl"/>
				<Item Name="LVParallelSchedulesEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVParallelSchedulesEnum.ctl"/>
				<Item Name="LVPlaneTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPlaneTypeDef.ctl"/>
				<Item Name="LVPoint32TypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPoint32TypeDef.ctl"/>
				<Item Name="LVPointAndBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointAndBoundsTypeDef.ctl"/>
				<Item Name="LVPointDoubleTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointDoubleTypeDef.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="LVPrintingMarginsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPrintingMarginsTypeDef.ctl"/>
				<Item Name="LVRangeTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRangeTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="LVRGBAColorTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRGBAColorTypeDef.ctl"/>
				<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
				<Item Name="LVRowAndColumnUnsignedTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnUnsignedTypeDef.ctl"/>
				<Item Name="LVScalePositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVScalePositionTypeDef.ctl"/>
				<Item Name="LVSceneTextAlignment.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVSceneTextAlignment.ctl"/>
				<Item Name="LVSelectionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVSelectionTypeDef.ctl"/>
				<Item Name="LVStringsAndValuesArrayTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVStringsAndValuesArrayTypeDef.ctl"/>
				<Item Name="LVTextColorsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVTextColorsTypeDef.ctl"/>
				<Item Name="LVTextureCoordinateArrayTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVTextureCoordinateArrayTypeDef.ctl"/>
				<Item Name="LVTickColorsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVTickColorsTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Not A Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="OffsetRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/OffsetRect.vi"/>
				<Item Name="Panel.lvlib" Type="Library" URL="/&lt;vilib&gt;/MGI/Panel Manager/Panel/Panel.lvlib"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Stall Data Flow.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/Stall Data Flow.vim"/>
				<Item Name="QuickDrop Parse Plugin Variant.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/QuickDropSupport/QuickDrop Parse Plugin Variant.vi"/>
				<Item Name="QuickDrop Plugin Data ver1.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/QuickDropSupport/QuickDrop Plugin Data ver1.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Property Browser VI Server Props" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">Property Browser VI Server Props</Property>
				<Property Name="Comments" Type="Str"></Property>
				<Property Name="DestinationID[0]" Type="Str">{BFA5DE79-57A5-4FE4-8466-05481790FD8E}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">1</Property>
				<Property Name="IncludedItems[0]" Type="Ref">/My Computer/All VI Server Class Get Properties</Property>
				<Property Name="IncludeProject" Type="Bool">false</Property>
				<Property Name="Path[0]" Type="Path">../Property Browser VI Server Props.zip</Property>
				<Property Name="ZipBase" Type="Str">NI_zipbasedefault</Property>
			</Item>
		</Item>
	</Item>
</Project>
